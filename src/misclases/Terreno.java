/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author wikip
 */
public class Terreno {
    private float ancho;
    private float largo;
    
    //metodos
    
    Terreno(){
        //constructor por omision
    this.ancho=0;
    this.largo=0;
    
    }
    
    Terreno(float ancho, float largo){
        this.ancho = ancho;
        this.largo = largo;
    }
    
    Terreno (Terreno x){
        //constructor por copia
    this.ancho = x.ancho;
    this.largo = x.largo;
        
            
    }
    
    //metodos set/get

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    
    //metodo de comportamiento
    
    public float calcularPerimetro(){
     float perimetro = 0.0f;
     perimetro = this.ancho * 2 + this.largo * 2;
     return perimetro;
    }
    
    public float calcularArea(){
     float area = 0.0f;
     area = this.ancho * this.largo;
     return area;
      
    }
    
    public void imprimirTerreno(){
        System.out.println(" Ancho = " + this.ancho);
        System.out.println(" Largo =" + this.largo);
        System.out.println(" El perimetro es " + this.calcularPerimetro());
        System.out.println(" El area es " + this.calcularArea());
    
        
    }
}
