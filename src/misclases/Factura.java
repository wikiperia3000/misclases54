/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author wikip
 */
public class Factura {
    
    private int numFactura;
    private String rfc;
    private String nombreCliente;
    private String domicilioFiscal;
    private String descripcion;
    private String fechaVenta;
    private float totalVenta;
    
    // constructores

    public Factura() {
        this.numFactura =0;
        this.rfc = "";
        this.nombreCliente = "";
        this.domicilioFiscal = "";
        this.descripcion = "";
        this.fechaVenta = "";
        this.totalVenta = 0.0f;
               
                
                
                
                
    }
        
        
        
   

    public Factura(int numFactura, String ref, String nombreCliente, String domicilioFiscal, String descripcion, String fechaVenta, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = ref;
        this.nombreCliente = nombreCliente;
        this.domicilioFiscal = domicilioFiscal;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
    public Factura (Factura otro){
        //Constructor por copia
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nombreCliente = otro.nombreCliente;
        this.domicilioFiscal = otro.domicilioFiscal;
        this.descripcion = otro.descripcion;
        this.fechaVenta = otro.fechaVenta;
        this.totalVenta = otro.totalVenta;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRef() {
        return rfc;
    }

    public void setRef(String ref) {
        this.rfc = ref;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
    
    
    public float calcularImpuesto (){
        
        float impuesto = 0.0f;
        impuesto = this.totalVenta * .16f;
       return impuesto;
    }
    
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.totalVenta + this.calcularImpuesto ();
        return total;
        
    }

    
    public void imprimirFactura(){
        System.out.println("Numero de factura : " + this.numFactura);
        System.out.println("RFC : " + this.rfc);
        System.out.println("Nombre del cliente : " + this.nombreCliente);
        System.out.println("Domicilio Fiscal : " + this.domicilioFiscal);
        System.out.println("Descripcion : " + this.descripcion);
        System.out.println("Fecha de venta : " + this.fechaVenta);
        System.out.println("Total de venta : " + this.totalVenta);
        System.out.println("Impuestos : " + this.calcularImpuesto());
        System.out.println("Total a pagar : "  this.calcularTotalPagar());
        
    }
    
            
    
    
    
    
    
    
    
}
